import React from 'react';

 export const HitListItems= (props) =>{
    return (
        <div>
               <span>Title : {props.title} </span>
               <span>URL : {props.url} </span>
               <span>Created At : {props.created_at} </span>
               <span>Author : {props.author} </span>
        </div>
    )
}

