import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom'
import './App.css';
import HomeComponent from './HomeComponent';
import DetailComponent from './DetailComponent';

function App() {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomeComponent} />
          <Route path="/detail" component={DetailComponent} />
        </Switch>
      </BrowserRouter>

  );
}

export default App;
