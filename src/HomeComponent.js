import React from 'react';
import './App.css';
class HomeComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            pageNumber: 0,
            dataList: [],
        }
    }

    componentDidMount() {
        document.addEventListener('scroll',this.trackScrolling);
        this.initializeInterval();
        this.calAPi(this.state.pageNumber);
    }

    componentWillUnmount() {
        this.clearInitializedInterval();
    }
    ItemClick=(item)=>{
       
        this.props.history.push({
            pathname:'/detail',
            state:{
                dataToBeShown:item
            }
        })
        // console.log("console lofg"+JSON.stringify(item));
    }
    render() {
        return (
            <div>
                <div className="page-header">
                    <span className="">Page Number {this.state.pageNumber}</span>
                </div>
                <div style={{paddingTop:60}}>

                    {
                        this.state.dataList.length > 0 ?

                            this.state.dataList.map((item, index) => {
                                return (
                                    <div key={index} >
                                        <div onClick={()=>this.ItemClick( this.state.dataList[index])} >
                                            <div >
                                                       <span>Title : {item.title} </span> 
                                            </div>
                                            <div>
                                                     <span>Created At : {item.created_at} </span>
                                                </div>
                                                <div>
                                                            <span>Author : {item.author} </span>
                                                </div>
                                                <div>
                                                       <span>URL : {item.url} </span>
                                                </div>
                                        </div>
                                        <div className="row-line"/>
                                      </div>
                                );
                            })
                            :
                            ""
                    }

                </div>`
                <div id="bottomId"></div>

            </div>
        )
    }


    trackScrolling = () => {
        const rootElement = document.getElementById('root');
        if (this.isBottom(rootElement)) {
            console.log("bottom print");
            this.clearInitializedInterval();
            this.setState({
                pageNumber: this.state.pageNumber + 1
            }, () => {
                this.calAPi(this.state.pageNumber)
                this.initializeInterval();
            })
        }
    }


    isBottom = (e1) => {
        return e1.getBoundingClientRect().bottom <= window.innerHeight;

    }

    calAPi(pageNumber) {
        fetch("https://hn.algolia.com/api/v1/search_by_date?tags=story&page=" + pageNumber).then(response => response.json())
            .then((response) => {
                var oldHitList = [...this.state.dataList];
                for (var j = 0; j < response.hits.length; j++) {
                    oldHitList.push(response.hits[j]);
                }
                this.setState({
                    dataList: oldHitList,
                });
            });

    }

    initializeInterval = () => {
        this.setAPIInterval = setInterval(() => {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                this.calAPi(this.state.pageNumber);
            });
        }, 10000);
    }

    clearInitializedInterval = () => {
        clearInterval(this.setAPIInterval);
    }

}


export default HomeComponent;
