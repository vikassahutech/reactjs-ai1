import React from 'react';
import './App.css';
class DetailComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataToBeShown: "",
        }
    }

    componentDidMount() {
        if (this.props.location.state !== undefined) {
            this.setState({
                dataToBeShown: this.props.location.state.dataToBeShown,
            });
        }
    }


    render() {
        return (
            <div>
                    {this.state.dataToBeShown ?
                        <div>
                            <span className="">{JSON.stringify(this.state.dataToBeShown)}</span>
                        </div>

                        : ""
                    }
            </div>
        )
    }


}


export default DetailComponent;
